package com.epam.lab3.game.model.units;

public class CloneSarge extends Soldier {

  public CloneSarge() {
    name = "Clone Sarge";
    maxHealthPoints = 170;
    healthPoints = maxHealthPoints;
    aggression = 40;
    defense = 30;
  }

  @Override
  public int getDamage() {
    this.damage = (int) (60 + 21 * Math.random());
    return damage;
  }
}
