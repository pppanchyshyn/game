package com.epam.lab3.game.model.units;

public interface Attackable {

  void attack(Soldier soldier);

  default void repair(Soldier soldier){}
}
