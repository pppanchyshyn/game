package com.epam.lab3.game.model.units;

public class CloneFootman extends Soldier {

  public CloneFootman() {
    name = "Clone Footman";
    maxHealthPoints = 140;
    healthPoints = maxHealthPoints;
    aggression = 50;
    defense = 50;
  }

  @Override
  public int getDamage() {
    this.damage = (int) (30 + 31 * Math.random());
    return damage;
  }
}
