package com.epam.lab3.game.model.units;

public class JediPeon extends Soldier {

  public JediPeon() {
    name = "Jedi Peon";
    maxHealthPoints = 150;
    healthPoints = maxHealthPoints;
    aggression = 40;
    defense = 50;
  }

  @Override
  public int getDamage() {
    this.damage = (int) (40 + 11 * Math.random());
    return damage;
  }

  public void attack(Soldier soldier) {
    int xp = soldier.getHealthPoints();
    int hurt;
    if (this.getAggression() > soldier.getDefense()) {
      hurt = (int) (this.getDamage() * (1 + 0.05 * (this.getAggression() - soldier.getDefense())));
      xp -= hurt;
    } else {
      hurt = (int) (this.getDamage() / (1 + 0.05 * (soldier.getDefense() - this.getAggression())));
      xp -= hurt;
    }
    int random =(int) (3 * Math.random());
    if(random == 0) {
      xp *= 1.5;
    }
    System.out.println(
        String.format("%s hit %d damage to %s\n", this.getName(), hurt, soldier.getName()));
    if (xp <= 0) {
      System.out.println(String.format("%s is died\n", soldier.getName()));
    }
    soldier.setHealthPoints(xp);
  }
}
