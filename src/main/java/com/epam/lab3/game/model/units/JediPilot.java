package com.epam.lab3.game.model.units;

public class JediPilot extends Soldier {

  public JediPilot() {
    name = "Jedi Pilot";
    maxHealthPoints = 180;
    healthPoints = maxHealthPoints;
    aggression = 60;
    defense = 50;
  }

  @Override
  public int getDamage() {
    this.damage = (int) (60 + 11 * Math.random());
    return damage;
  }
}
