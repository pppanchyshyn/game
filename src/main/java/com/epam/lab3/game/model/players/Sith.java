package com.epam.lab3.game.model.players;

import com.epam.lab3.game.model.units.CloneDoctor;
import com.epam.lab3.game.model.units.CloneFootman;
import com.epam.lab3.game.model.units.CloneSarge;

public class Sith extends Player {

  public Sith(String name) {
    type = "Sith";
    this.name = name;
    soldiers.add(new CloneFootman());
    soldiers.add(new CloneSarge());
    soldiers.add(new CloneDoctor());
  }
}
