package com.epam.lab3.game.model.units;

public abstract class Medic extends Soldier {

  public void repair(Soldier soldier) {
    int xp = soldier.getHealthPoints();
    int reparation;
    reparation = (int) (this.getDamage() * 1.2);
    xp += reparation;
    System.out
        .println(String
            .format("%s repair %d XP to %s\n", this.getName(), reparation, soldier.getName()));
    if (xp > soldier.getMaxHealthPoints()) {
      soldier.setHealthPoints(soldier.getMaxHealthPoints());
    } else {
      soldier.setHealthPoints(xp);
    }
  }
}
