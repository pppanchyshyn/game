package com.epam.lab3.game.model.units;

public class JediHealer extends Medic {

  public JediHealer() {
    name = "Jedi Healer";
    maxHealthPoints = 200;
    healthPoints = maxHealthPoints;
    aggression = 30;
    defense = 70;
    multiActioned = true;
  }

  @Override
  public int getDamage() {
    this.damage = (int) (50 + 11 * Math.random());
    return damage;
  }
}
