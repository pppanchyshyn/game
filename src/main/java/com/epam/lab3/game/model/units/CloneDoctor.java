package com.epam.lab3.game.model.units;

public class CloneDoctor extends Medic{

  public CloneDoctor() {
    name = "Clone Doctor";
    maxHealthPoints = 180;
    healthPoints = maxHealthPoints;
    aggression = 50;
    defense = 60;
    multiActioned = true;
  }

  @Override
  public int getDamage() {
    this.damage = (int) (40 + 51 * Math.random());
    return damage;
  }
}
