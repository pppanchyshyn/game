package com.epam.lab3.game.model.players;

import com.epam.lab3.game.model.units.JediHealer;
import com.epam.lab3.game.model.units.JediPeon;
import com.epam.lab3.game.model.units.JediPilot;

public class Jedi extends Player {

  public Jedi(String name) {
    type = "Jedi";
    this.name = name;
    soldiers.add(new JediPeon());
    soldiers.add(new JediPilot());
    soldiers.add(new JediHealer());
  }
}
