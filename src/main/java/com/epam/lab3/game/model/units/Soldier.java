package com.epam.lab3.game.model.units;

public abstract class Soldier implements Attackable {

  protected String name = "Soldier";
  int maxHealthPoints;
  int healthPoints;
  int aggression;
  int defense;
  int damage;
  boolean multiActioned;

  public String getName() {
    return name;
  }

  public int getMaxHealthPoints() {
    return maxHealthPoints;
  }

  public int getHealthPoints() {
    return healthPoints;
  }

  public void setHealthPoints(int healthPoints) {
    this.healthPoints = healthPoints;
  }

  public int getAggression() {
    return aggression;
  }

  public int getDefense() {
    return defense;
  }

  public int getDamage() {
    return damage;
  }

  public boolean isMultiActioned() {
    return multiActioned;
  }

  @Override
  public String toString() {
    return String.format("%-15s%-5d%-13d%-10d%-10d",
        getName(), getHealthPoints(), getAggression(), getDefense(), getDamage());
  }

  public void attack(Soldier soldier) {
    int xp = soldier.getHealthPoints();
    int hurt;
    if (this.getAggression() > soldier.getDefense()) {
      hurt = (int) (this.getDamage() * (1 + 0.05 * (this.getAggression() - soldier.getDefense())));
      xp -= hurt;
    } else {
      hurt = (int) (this.getDamage() / (1 + 0.05 * (soldier.getDefense() - this.getAggression())));
      xp -= hurt;
    }
    System.out.println(
        String.format("%s hit %d damage to %s\n", this.getName(), hurt, soldier.getName()));
    if (xp <= 0) {
      System.out.println(String.format("%s is died\n", soldier.getName()));
    }
    soldier.setHealthPoints(xp);
  }
}