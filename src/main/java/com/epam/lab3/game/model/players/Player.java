package com.epam.lab3.game.model.players;

import com.epam.lab3.game.model.units.Soldier;
import java.util.LinkedList;

public abstract class Player {

  protected String name;
  String type;
  LinkedList<Soldier> soldiers = new LinkedList<>();

  public String getName() {
    return name;
  }

  public String getType() {
    return type;
  }

  public LinkedList<Soldier> getSoldiers() {
    return soldiers;
  }
}
