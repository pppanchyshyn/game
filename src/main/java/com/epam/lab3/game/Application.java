package com.epam.lab3.game;

import com.epam.lab3.game.controller.Controller;

public class Application {

  public static void main(String[] args) {
    Controller controller= new Controller();
    controller.execute();
  }
}
