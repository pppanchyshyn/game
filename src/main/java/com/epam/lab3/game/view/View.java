package com.epam.lab3.game.view;

import com.epam.lab3.game.model.units.Soldier;
import com.epam.lab3.game.model.players.Player;
import java.util.List;
import java.util.Scanner;

public class View {

  private Scanner input = new Scanner(System.in);

  private int enterInteger() {
    int integer;
    while (true) {
      if (input.hasNextInt()) {
        integer = input.nextInt();
        break;
      } else {
        System.out.print("Not integer! Try again: ");
        input.nextLine();
      }
    }
    return integer;
  }

  private int enterMenuItem(int finishValue) {
    int menuItem;
    while (true) {
      menuItem = enterInteger();
      if ((menuItem >= 0) && (menuItem < finishValue)) {
        break;
      } else {
        System.out.print("Non-existent menu item. Try again: ");
        input.nextLine();
      }
    }
    return menuItem;
  }

  public String initialize() {
    System.out.print("Welcome to Battle for the galaxy "
        + "\nTell your name, stranger ");
    return input.next();
  }

  public int chooseSide() {
    System.out.println("Select your side"
        + "\nTo join the Sitches press 0"
        + "\nTo join the Jedi press 1");
    return enterMenuItem(2);
  }

  public int chooseAction() {
    System.out.println("Choose action"
        + "\nTo attack enemy press 0"
        + "\nTo treat friend press 1");
    return enterMenuItem(2);
  }

  public int chooseTarget(List<Soldier> soldiers) {
    for (int i = 0; i < soldiers.size(); i++) {
      System.out.println(String.format("To attack %s press %d", soldiers.get(i).getName(), i));
    }
    return enterMenuItem(soldiers.size());
  }

  public int chooseFriend(List<Soldier> soldiers) {
    for (int i = 0; i < soldiers.size(); i++) {
      System.out.println(String.format("To hill %s press %d", soldiers.get(i).getName(), i));
    }
    return enterMenuItem(soldiers.size());
  }

  public void showTurnOrder(Player player, int i) {
    System.out.println(String.format("%s %s 's %s turn: ", player.getType(), player.getName(),
        player.getSoldiers().get(i).getName()));
  }
}
