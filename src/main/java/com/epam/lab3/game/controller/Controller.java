package com.epam.lab3.game.controller;

import com.epam.lab3.game.model.units.Soldier;
import com.epam.lab3.game.model.players.Jedi;
import com.epam.lab3.game.model.players.Player;
import com.epam.lab3.game.model.players.Sith;
import com.epam.lab3.game.view.View;
import java.util.List;

public class Controller {

  private View view = new View();

  public void execute() {
    String name1 = view.initialize();
    Player player1 = createPlayer(name1);
    showArmy(player1);
    String name2 = view.initialize();
    Player player2 = createPlayer(name2);
    showArmy(player2);
    boolean stop = false;
    while (!stop) {
      for (int i = 0, j = 0; i < player1.getSoldiers().size() && j < player2.getSoldiers().size();
          i++, j++) {
        if (player1.getSoldiers().size() < i + 1) {
          i--;
        }
        view.showTurnOrder(player1, i);
        makeMove(player1, player2, i);
        stop = isItStop(player2);
        if (stop) {
          break;
        }
        showArmy(player2);
        if (player2.getSoldiers().size() < j + 1) {
          j--;
        }
        view.showTurnOrder(player2, j);
        makeMove(player2, player1, j);
        stop = isItStop(player1);
        if (stop) {
          break;
        }
        showArmy((player1));
      }
    }
  }

  private Player createPlayer(String name) {
    Player player;
    int side = view.chooseSide();
    if (side == 1) {
      player = new Jedi(name);
    } else {
      player = new Sith(name);
    }
    return player;
  }

  private void showArmy(Player player) {
    System.out.printf("%-15s%-5s%-13s%-10s%-10s\n",
        "Name", "XP", "Aggression", "Defence", "Damage");
    for (Soldier soldier : player.getSoldiers()) {
      System.out.println(soldier.toString());
    }
    System.out.println(" ");
  }

  private void removeDead(List<Soldier> soldiers) {
    for (int i = 0; i < soldiers.size(); i++) {
      if (soldiers.get(i).getHealthPoints() <= 0) {
        soldiers.remove(i);
      }
    }
  }

  private void makeMove(Player player1, Player player2, int iterator) {
    if (player1.getSoldiers().get(iterator).isMultiActioned()) {
      if (view.chooseAction() == 0) {
        player1.getSoldiers().get(iterator)
            .attack(player2.getSoldiers().get(view.chooseTarget(player2.getSoldiers())));
        removeDead(player2.getSoldiers());
      } else {
        player1.getSoldiers().get(iterator)
            .repair(player1.getSoldiers().get(view.chooseFriend(player1.getSoldiers())));
      }
    } else {
      player1.getSoldiers().get(iterator)
          .attack(player2.getSoldiers().get(view.chooseTarget(player2.getSoldiers())));
      removeDead(player2.getSoldiers());
    }
  }

  private boolean isItStop(Player player) {
    if (player.getSoldiers().isEmpty()) {
      System.out.println(String.format("%s won the battle", player.getName()));
      return true;
    } else {
      return false;
    }
  }
}